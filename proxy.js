exports.handler = async (event, context) => {
    try {
      const response = await fetch('https://js.usemessages.com/conversations-embed.js', {
        headers: {
          'Access-Control-Allow-Origin': '*',
          'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept',
        },
      });
  
      if (!response.ok) {
        throw new Error('Failed to fetch the resource.');
      }
  
      const content = await response.text();
  
      return {
        statusCode: 200,
        headers: {
          'Content-Type': 'text/javascript',
        },
        body: content,
      };
    } catch (error) {
      return {
        statusCode: 500,
        body: JSON.stringify({ error: 'Something went wrong' }),
      };
    }
  };
  